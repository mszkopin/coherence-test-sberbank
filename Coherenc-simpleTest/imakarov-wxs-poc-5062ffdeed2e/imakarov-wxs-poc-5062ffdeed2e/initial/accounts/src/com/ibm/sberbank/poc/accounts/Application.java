package com.ibm.sberbank.poc.accounts;

import java.util.logging.Logger;

import com.ibm.sberbank.poc.accounts.client.AccountGenerator;
import com.ibm.sberbank.poc.accounts.client.TransactionGenerator;
import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.websphere.objectgrid.NoActiveTransactionException;
import com.ibm.websphere.objectgrid.ObjectGrid;
import com.ibm.websphere.objectgrid.ObjectGridException;
import com.ibm.websphere.objectgrid.ObjectGridManagerFactory;
import com.ibm.websphere.objectgrid.Session;
import com.ibm.websphere.objectgrid.TransactionAlreadyActiveException;
import com.ibm.websphere.objectgrid.TransactionException;
import com.ibm.websphere.objectgrid.em.EntityManager;
import com.ibm.websphere.objectgrid.em.Query;
import com.ibm.websphere.objectgrid.plugins.TransactionCallbackException;

public class Application {

	private static ObjectGrid og;
	private static int counter = 0;

	public static void main(String[] args) throws Exception {
		og = ObjectGridManagerFactory.getObjectGridManager().createObjectGrid("Account");
		og.registerEntities(new Class[] {Account.class});

		AccountGenerator.getInstance().generate(200000);
		
		showStatInternal();

		new Thread(new TransactionGenerator(3200)).start();
		new Thread(new TransactionGenerator(3200)).start();
		new Thread(new TransactionGenerator(3200)).start();
		new Thread(new TransactionGenerator(3200)).start();		
	}

	public static void showStat() throws ObjectGridException,
			TransactionCallbackException, TransactionAlreadyActiveException,
			TransactionException, NoActiveTransactionException {
		counter++;
		if (counter < 4)
			return;
		showStatInternal();
	}

	private static void showStatInternal() throws ObjectGridException,
			TransactionCallbackException {
		Session s = og.getSession();
		EntityManager em = s.getEntityManager();
		em.getTransaction().begin();
		
		Query query = em.createQuery("select count(acc) from Account acc where acc.balance <> acc.initial");
			Logger.getLogger(Application.class.getName()).info("Affected accounts: " + query.getSingleResult());
		em.getTransaction().commit();
	}

}
