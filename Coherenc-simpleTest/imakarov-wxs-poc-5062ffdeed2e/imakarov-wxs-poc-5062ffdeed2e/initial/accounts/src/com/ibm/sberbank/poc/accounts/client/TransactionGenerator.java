package com.ibm.sberbank.poc.accounts.client;

import java.util.logging.Logger;

import com.ibm.sberbank.poc.accounts.Application;
import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.websphere.objectgrid.ObjectGrid;
import com.ibm.websphere.objectgrid.ObjectGridManagerFactory;
import com.ibm.websphere.objectgrid.Session;
import com.ibm.websphere.objectgrid.em.EntityManager;

public class TransactionGenerator implements Runnable {

	private int count;

	public TransactionGenerator(int count) {
		this.count = count;
		
	}
	
	@Override
	public void run() {
		ObjectGrid og = ObjectGridManagerFactory.getObjectGridManager().getObjectGrid("Account");
		try {
			Session session = og.getSession();
			EntityManager em = session.getEntityManager();
			for (int i = 0; i < count; i++) {
				try {
					em.getTransaction().begin();
					transfer(em);
					em.getTransaction().commit();
				} catch (Exception e) {
					i--;
//					e.printStackTrace();
				}
			}
			Logger.getLogger(TransactionGenerator.class.getName()).info("Succesfully ended");
			Application.showStat();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void transfer(EntityManager em) throws Exception {
		AccountGenerator accountGenerator = AccountGenerator.getInstance();
		String from = accountGenerator.randomAccountId();
		String to = accountGenerator.randomAccountId();
		
		while (from.equals(to))
			to = accountGenerator.randomAccountId();
		
		Account accountTo = (Account) em.findForUpdate(Account.class, to);
		Account accountFrom = (Account) em.findForUpdate(Account.class, from);
		double balance = accountGenerator.nextBalance();
		
		accountFrom.add(-balance);
		accountTo.add(balance);
	}
	
	
	
}
