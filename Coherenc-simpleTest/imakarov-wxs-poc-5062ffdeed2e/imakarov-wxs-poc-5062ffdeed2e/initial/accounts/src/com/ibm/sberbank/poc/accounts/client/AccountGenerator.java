package com.ibm.sberbank.poc.accounts.client;

import java.util.Random;

import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.websphere.objectgrid.ObjectGrid;
import com.ibm.websphere.objectgrid.ObjectGridException;
import com.ibm.websphere.objectgrid.ObjectGridManagerFactory;
import com.ibm.websphere.objectgrid.Session;
import com.ibm.websphere.objectgrid.em.EntityManager;
import com.ibm.websphere.objectgrid.plugins.TransactionCallbackException;

public class AccountGenerator {

	private static AccountGenerator INSTANCE = new AccountGenerator();
	private int sequence = 1000000000;
	private final Random random = new Random();
	
	private AccountGenerator() {}
	
	public static AccountGenerator getInstance() {
		return INSTANCE;
	}
	
	public void generate(int count) throws TransactionCallbackException, ObjectGridException {
		ObjectGrid og = ObjectGridManagerFactory.getObjectGridManager().getObjectGrid("Account");
		Session session = og.getSession();
		EntityManager em = session.getEntityManager();
		
		for (int i = 0; i < count; i++) {
			em.getTransaction().begin();
			Account account = nextAccount();
			em.persist(account);
			em.getTransaction().commit();
		}
	}
	
	public Account nextAccount() {
		Account account = new Account();
		account.setId("account-id-" + sequence++);
		account.setBalance(nextBalance());
		return account;
	}
	
	public double nextBalance() {
		return random.nextDouble() * 10000000;
	}
	
	public String randomAccountId() {
		int accId = 1000000000 + random.nextInt(sequence - 1000000000);
		return "account-id-" + accId;
	}
}
