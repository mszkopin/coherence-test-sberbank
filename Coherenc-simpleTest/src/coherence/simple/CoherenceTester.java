package coherence.simple;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.ibm.sberbank.poc.accounts.client.AccountGenerator;
import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.sberbank.poc.accounts.model.Transaction;
import com.ibm.sberbank.poc.processor.TransactionProcessor;
import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;


public class CoherenceTester {

	public static int ACCOUNTS = 4000;
	public static int TRANSACTIONS = 1000;
	
	private long startTimer;
	
	public void startTimer() {
		this.startTimer = System.currentTimeMillis();
	}
	
	public long stopAndShowTimer() {
		return System.currentTimeMillis() - startTimer;
	}
	
	public static void main(String []args) {		
		CoherenceTester tester = new CoherenceTester();
		
		System.out.println("Starting Coherence test client");
		CacheFactory.ensureCluster();
	    NamedCache cache = CacheFactory.getCache("positions");
	    System.out.println("Initial Cache size: " + cache.size());
	    
	    
	    
	    System.out.println("Generating " + CoherenceTester.ACCOUNTS + " accounts ...");
	    tester.startTimer();
	    
	    AccountGenerator generator = AccountGenerator.getInstance();
	    
	    ConcurrentHashMap<String, Account> accounts = generator.generateAccounts(CoherenceTester.ACCOUNTS);
	    System.out.println("Generation accounts lasted: " + tester.stopAndShowTimer() + " [ms]" );
	    
	    System.out.println("Adding accounts to cache ....");
	    tester.startTimer();
	    
	    cache.putAll(accounts);
	    
	    System.out.println("Adding accounts to cache lasted: " + tester.stopAndShowTimer() + " [ms]");
	    
	    
	    System.out.println("Final Cache size: " + cache.size());
	    
	    System.out.println("Generating " + CoherenceTester.TRANSACTIONS + " transactions ...");
	    tester.startTimer();
	    
	    HashMap<String, ArrayList<Transaction>> transactions = generator.generateTransactions(CoherenceTester.TRANSACTIONS);
	    
	    System.out.println("Transactions generated in " + tester.stopAndShowTimer() + " [ms]");
	    System.out.println("Total different accounts affected: " + transactions.size());
	    
	    System.out.println("Sending Processor to the CACHE ...");
	    tester.startTimer();
	    
	    cache.invokeAll(transactions.keySet(), new TransactionProcessor(transactions));
	    
	    System.out.println("Processing finished in " + tester.stopAndShowTimer());
	    
	    // STANDARD IMPLEMENTATION OF TRANSACION APPLICATION
//	    System.out.println("Standard implementation of put and get");
//	    tester.startTimer();
//	    Set<String> accountSet = transactions.keySet();
//	    for(Iterator<String> itr = accountSet.iterator(); itr.hasNext();) {
//	    	String accountID = itr.next();
//	    	Account acc = (Account) cache.get(accountID);
//	    	for(Iterator<Transaction> trsItr = transactions.get(accountID).iterator(); trsItr.hasNext();) {
//	    		Transaction tr = trsItr.next(); 
//	    		if(acc.getId().equals(tr.getFrom())) {
//	    			acc.add(-tr.getBalance());
//	    		} else {
//	    			acc.add(tr.getBalance());
//	    		}	
//	    	}
//	    	cache.put(accountID, acc);
//	    }
//	    System.out.println("Standard transaction application took " + tester.stopAndShowTimer());
//	    
	    CacheFactory.shutdown();
	    
	    
	    System.out.println("Exit");
	    
	}

}
