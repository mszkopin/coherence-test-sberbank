package com.ibm.sberbank.poc.accounts.model;

import java.io.Serializable;




public class Transaction implements Serializable {
	
	private static final long serialVersionUID = 1L;


	private String from;
	
	
	private String to;
	
	
	double balance;
	
	public Transaction(String from, String to, double balance) {
		this.from = from;
		this.to = to;
		this.balance = balance;
	}
	
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	
	
	
}
