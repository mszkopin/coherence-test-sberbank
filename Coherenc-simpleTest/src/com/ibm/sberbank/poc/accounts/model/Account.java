package com.ibm.sberbank.poc.accounts.model;

import java.io.Serializable;

 
public class Account implements Serializable {
	

	private static final long serialVersionUID = 1L;

	String id;

	String additionalInfo;

	double balance;

	double initial = 0.0;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
		if (initial == 0.0)
			initial = balance;
	}
	
	public double getInitial() {
		return initial;
	}
	
	public void setInitial(double initial) {
		this.initial = initial;
	}
	
	public void add(double sum) {
		balance += sum;
	}
	
	
	
	
}
