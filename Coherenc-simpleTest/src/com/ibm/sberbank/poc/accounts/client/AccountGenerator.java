package com.ibm.sberbank.poc.accounts.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.sberbank.poc.accounts.model.Transaction;


public class AccountGenerator {

	private static int START_SEQ_ID = 1000000000;
	private static AccountGenerator INSTANCE = new AccountGenerator();
	
	private int sequence = START_SEQ_ID;
	private final Random random = new Random();
	
	private AccountGenerator() {}
	
	public static AccountGenerator getInstance() {
		return INSTANCE;
	}
	
	public ConcurrentHashMap<String, Account> generateAccounts(int count) {
		ConcurrentHashMap<String, Account> generatedAcc = new ConcurrentHashMap<String, Account>(count);
		Account acc;
		for (int i = 0; i < count; i++) {
			acc = nextAccount();
			generatedAcc.put(acc.getId(), acc);
		}
		return generatedAcc;
	}
	
	public HashMap<String, ArrayList<Transaction>> generateTransactions(int count) {
		HashMap<String, ArrayList<Transaction>> transactions = new HashMap<String, ArrayList<Transaction>>(count*2);
		for (int i=0; i<count; i++) {
			String from = randomAccountId();
			String to = randomAccountId();
			
			while (from.equals(to)) {
				to = randomAccountId();
			}
			double balance = nextBalance();
			Transaction tr = new Transaction(from, to, balance);
			
			if(!transactions.containsKey(from)) {
				ArrayList<Transaction> listAcc = new ArrayList<Transaction>(count/1000);
				listAcc.add(tr);
				transactions.put(from, listAcc);
			} else {
				transactions.get(from).add(tr);
			}
			if(!transactions.containsKey(to)) {
				ArrayList<Transaction> listAcc = new ArrayList<Transaction>(count/1000);
				listAcc.add(tr);
				transactions.put(to, listAcc);
			} else {
				transactions.get(to).add(tr);
			}
		}
		return transactions;
	}
	
	protected Account nextAccount() {
		Account account = new Account();
		account.setId("account-id-" + sequence++);
		account.setBalance(nextBalance());
		return account;
	}
	
	protected double nextBalance() {
		return random.nextDouble() * 10000000;
	}
	
	protected String randomAccountId() {
		int accId = START_SEQ_ID + random.nextInt(sequence - START_SEQ_ID);
		return "account-id-" + accId;
	}
}
