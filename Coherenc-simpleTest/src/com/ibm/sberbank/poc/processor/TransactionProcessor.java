package com.ibm.sberbank.poc.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.ibm.sberbank.poc.accounts.model.Account;
import com.ibm.sberbank.poc.accounts.model.Transaction;
import com.tangosol.util.InvocableMap.Entry;
import com.tangosol.util.processor.AbstractProcessor;


public class TransactionProcessor extends AbstractProcessor  {
	
	
	private static final long serialVersionUID = 1L;
	
	
	protected HashMap<String, ArrayList<Transaction>> transMap;
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public TransactionProcessor(HashMap<String, ArrayList<Transaction>> transMap) {
		this.transMap = transMap;
	}
	
	
	@Override
	public Object process(Entry entry) {
		Account acc = (Account) entry.getValue();
		
		//System.out.println("Processing account id: " + acc.getId());
		
		ArrayList<Transaction> transactions = transMap.get(acc.getId());
		
		//System.out.println("Found " + transactions.size() + " transactions for this account");
		
		for(Iterator<Transaction> iter = transactions.iterator(); iter.hasNext(); ) {
		   Transaction tr = iter.next();
		   if(acc.getId().equals(tr.getFrom())) {
			   acc.add(-tr.getBalance());
		   } else {
			   acc.add(tr.getBalance());
		   }
		}
		
		entry.setValue(acc);
		return null;
	}

/*
	@Override
	public void readExternal(PofReader in) throws IOException {
		// TODO Auto-generated method stub
		//serialVersionUID =  in.readLong(0);
		int size = in.readInt(0);
		
		ArrayList<String> keySet = new ArrayList<String>(size);
		transMap = new HashMap<String, ArrayList<Transaction>>(size);
		
		keySet = (ArrayList<String>) in.readCollection(1, keySet);
		
//		int i = 2;
//		for(Iterator<String> itr = keySet.iterator(); itr.hasNext();) {
//			transMap.put(itr.next(), (ArrayList<Transaction>) in.readCollection(i++,null));  
//		}
	}


	@Override
	public void writeExternal(PofWriter out) throws IOException {
		// TODO Auto-generated method stub
		//out.writeMap(0, transMap, String.class, ArrayList.class);
		Set<String> keySet = transMap.keySet();
		
		out.writeInt(0, keySet.size());
		
		
		out.writeCollection(1, transMap.keySet(), String.class);
//		int i=2;
//		for(Iterator<String> itr = keySet.iterator(); itr.hasNext();) {
//			out.writeCollection(i++, transMap.get(itr.next()));
//		}
	}
*/
}
